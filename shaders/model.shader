[[FX]]

// Supported Flags
/* ---------------
	_F01_Skinning
	_F02_NormalMapping
	_F03_ParallaxMapping
	_F04_EnvMapping
	_F05_AlphaTest
	_F06_HighQualShadows
*/


// Samplers
sampler2D albedoMap = sampler_state
{
	Texture = "textures/common/white.tga";
};

sampler2D normalMap = sampler_state
{
	Texture = "textures/common/defnorm.tga";
};

samplerCube ambientMap = sampler_state
{
	Address = Clamp;
	Filter = Bilinear;
	MaxAnisotropy = 1;
};

samplerCube envMap = sampler_state
{
	Address = Clamp;
	Filter = Bilinear;
	MaxAnisotropy = 1;
};

// Uniforms
float4 matDiffuseCol <
	string desc_abc = "abc: diffuse color";
	string desc_d   = "d: alpha for opacity";
> = {1.0, 1.0, 1.0, 1.0};

float4 matSpecParams <
	string desc_abc = "abc: specular color";
	string desc_d   = "d: gloss";
> = {0.005, 0.005, 0.005, 0.5};

float4 matReflParams <
	string desc_abc = "abc: strength in RGB";
	string desc_d   = "d: diffusion (0..4)";
> = {0.3, 0.3, 0.3, 3.0};

float4 matReflSpreadParams <
	string desc_ab  = "ab: diffusion spread in u";
	string desc_cd  = "cd: diffusion spread in w";
> = {0.3, 0.3, 0.3, 0.3};

float4 matParallaxParams <
	string desc_a   = "a: parallax scale";
	string desc_b   = "b: parallax bias";
> = {0.02, -0.005, 0.0, 0.0};

// Contexts
context ATTRIBPASS
{
	VertexShader = compile GLSL VS_GENERAL;
	PixelShader = compile GLSL FS_ATTRIBPASS;
}

context SHADOWMAP
{
	VertexShader = compile GLSL VS_SHADOWMAP;
	PixelShader = compile GLSL FS_SHADOWMAP;
}

context LIGHTING
{
	VertexShader = compile GLSL VS_GENERAL;
	PixelShader = compile GLSL FS_LIGHTING;

	ZWriteEnable = false;
	BlendMode = Add;
}

context AMBIENT
{
	VertexShader = compile GLSL VS_GENERAL;
	PixelShader = compile GLSL FS_AMBIENT;
}


[[VS_GENERAL]]
// =================================================================================================

#ifdef _F03_ParallaxMapping
	#define _F02_NormalMapping
#endif

#include "shaders/utilityLib/vertCommon.glsl"

#ifdef _F01_Skinning
	#include "shaders/utilityLib/vertSkinning.glsl"
#endif

uniform mat4 viewProjMat;
uniform vec3 viewerPos;
attribute vec3 vertPos;
attribute vec2 texCoords0;
attribute vec3 normal;

#ifdef _F02_NormalMapping
	attribute vec4 tangent;
#endif

varying vec4 pos, vsPos;
varying vec2 texCoords;

#ifdef _F02_NormalMapping
	varying mat3 tsbMat;
#else
	varying vec3 tsbNormal;
#endif

#ifdef _F03_ParallaxMapping
	uniform vec4 matParallaxParams;
	varying vec3 eyeTS;
#endif


void main( void )
{
#ifdef _F01_Skinning
	mat4 skinningMat = calcSkinningMat();
	mat3 skinningMatVec = getSkinningMatVec( skinningMat );
#endif

	// Calculate normal
#ifdef _F01_Skinning
	vec3 _normal = normalize( calcWorldVec( skinVec( normal, skinningMatVec ) ) );
#else
	vec3 _normal = normalize( calcWorldVec( normal ) );
#endif

	// Calculate tangent and bitangent
#ifdef _F02_NormalMapping
	#ifdef _F01_Skinning
		vec3 _tangent = normalize( calcWorldVec( skinVec( tangent.xyz, skinningMatVec ) ) );
	#else
		vec3 _tangent = normalize( calcWorldVec( tangent.xyz ) );
	#endif

	vec3 _bitangent = cross( _normal, _tangent ) * tangent.w;
	tsbMat = calcTanToWorldMat( _tangent, _bitangent, _normal );
#else
	tsbNormal = _normal;
#endif

	// Calculate world space position
#ifdef _F01_Skinning
	pos = calcWorldPos( skinPos( vec4( vertPos, 1.0 ), skinningMat ) );
#else
	pos = calcWorldPos( vec4( vertPos, 1.0 ) );
#endif

	vsPos = calcViewPos( pos );

	// Calculate tangent space eye vector
#ifdef _F03_ParallaxMapping
	eyeTS = calcTanVec( viewerPos - pos.xyz, _tangent, _bitangent, _normal );
#endif

	// Calculate texture coordinates and clip space position
	texCoords = texCoords0;

	// Flip texture vertically to match the GL coordinate system
	texCoords.t *= -1.0;

	gl_Position = viewProjMat * pos;
}


[[FS_ATTRIBPASS]]
// =================================================================================================

#ifdef _F03_ParallaxMapping
	#define _F02_NormalMapping
#endif

#include "shaders/utilityLib/fragDeferredWrite.glsl"

uniform vec3 viewerPos;
uniform vec4 matDiffuseCol;
uniform vec4 matSpecParams;
uniform sampler2D albedoMap;

#ifdef _F02_NormalMapping
	uniform sampler2D normalMap;
#endif

varying vec4 pos;
varying vec2 texCoords;

#ifdef _F02_NormalMapping
	varying mat3 tsbMat;
#else
	varying vec3 tsbNormal;
#endif

#ifdef _F03_ParallaxMapping
	uniform vec4 matParallaxParams;
	varying vec3 eyeTS;
#else
	#define newCoords texCoords
#endif

void main( void )
{
#ifdef _F03_ParallaxMapping
	vec3 newCoords = vec3( texCoords, 0 );

	// Iterative parallax mapping
	vec3 eye = normalize( eyeTS );
	for( int i = 0; i < 4; ++i )
	{
		vec4 nmap = texture2D( normalMap, newCoords.st );
		float height = nmap.a * matParallaxParams.b + matParallaxParams.b;
		newCoords += (height - newCoords.p) * nmap.z * eye;
	}
#endif

	vec4 albedo = texture2D( albedoMap, newCoords.st ) * matDiffuseCol;

#ifdef _F05_AlphaTest
	if( albedo.a < 0.01 ) discard;
#endif

#ifdef _F02_NormalMapping
	vec3 normalMap = texture2D( normalMap, newCoords.st ).rgb * 2.0 - 1.0;
	vec3 normal = tsbMat * normalMap;
#else
	vec3 normal = tsbNormal;
#endif

	vec3 newPos = pos.xyz;

#ifdef _F03_ParallaxMapping
	newPos += vec3( 0.0, newCoords.p, 0.0 );
#endif

	setMatID( 1.0 );
	setPos( newPos - viewerPos );
	setNormal( normalize( normal ) );
	setAlbedo( albedo.rgb );
	setSpecParams( matSpecParams.rgb, matSpecParams.a );
}


[[VS_SHADOWMAP]]
// =================================================================================================

#include "shaders/utilityLib/vertCommon.glsl"
#include "shaders/utilityLib/vertSkinning.glsl"

uniform mat4 viewProjMat;
uniform vec4 lightPos;
attribute vec3 vertPos;
varying vec3 lightVec;

#ifdef _F05_AlphaTest
	attribute vec2 texCoords0;
	varying vec2 texCoords;
#endif

void main( void )
{
#ifdef _F01_Skinning
	vec4 pos = calcWorldPos( skinPos( vec4( vertPos, 1.0 ) ) );
#else
	vec4 pos = calcWorldPos( vec4( vertPos, 1.0 ) );
#endif

#ifdef _F05_AlphaTest
	texCoords = texCoords0;
#endif

	lightVec = lightPos.xyz - pos.xyz;
	gl_Position = viewProjMat * pos;
}


[[FS_SHADOWMAP]]
// =================================================================================================

uniform vec4 lightPos;
uniform float shadowBias;
varying vec3 lightVec;

#ifdef _F05_AlphaTest
	uniform vec4 matDiffuseCol;
	uniform sampler2D albedoMap;
	varying vec2 texCoords;
#endif

void main( void )
{
#ifdef _F05_AlphaTest
	vec4 albedo = texture2D( albedoMap, texCoords ) * matDiffuseCol;
	if( albedo.a < 0.01 ) discard;
#endif

	float dist = length( lightVec ) / lightPos.w;
	gl_FragDepth = dist
#ifdef _F06_HighQualShadows
			// Clearly better bias but requires SM 3.0
			+ abs( dFdx( dist ) ) + abs( dFdy( dist ) )
#endif
			+ shadowBias;
}


[[FS_LIGHTING]]
// =================================================================================================

#ifdef _F03_ParallaxMapping
	#define _F02_NormalMapping
#endif

#include "shaders/utilityLib/fragLighting.glsl"

uniform vec4 matDiffuseCol;
uniform vec4 matSpecParams;
uniform sampler2D albedoMap;

#ifdef _F02_NormalMapping
	uniform sampler2D normalMap;
#endif

varying vec4 pos, vsPos;
varying vec2 texCoords;

#ifdef _F02_NormalMapping
	varying mat3 tsbMat;
#else
	varying vec3 tsbNormal;
#endif

#ifdef _F03_ParallaxMapping
	uniform vec4 matParallaxParams;
	varying vec3 eyeTS;
#else
	#define newCoords texCoords
#endif

void main( void )
{
#ifdef _F03_ParallaxMapping
	vec3 newCoords = vec3( texCoords, 0 );

	// Iterative parallax mapping
	vec3 eye = normalize( eyeTS );
	for( int i = 0; i < 4; ++i )
	{
		vec4 nmap = texture2D( normalMap, newCoords.st );
		float height = nmap.a * matParallaxParams.b + matParallaxParams.b;
		newCoords += (height - newCoords.p) * nmap.z * eye;
	}
#endif

	vec4 albedo = texture2D( albedoMap, newCoords.st ) * matDiffuseCol;

#ifdef _F05_AlphaTest
	if( albedo.a < 0.01 ) discard;
#endif

#ifdef _F02_NormalMapping
	vec3 normalMap = texture2D( normalMap, newCoords.st ).rgb * 2.0 - 1.0;
	vec3 normal = tsbMat * normalMap;
#else
	vec3 normal = tsbNormal;
#endif

	vec3 newPos = pos.xyz;

#ifdef _F03_ParallaxMapping
	newPos += vec3( 0.0, newCoords.p, 0.0 );
#endif

	gl_FragColor.rgb =
		calcPhongSpotLight( newPos, normalize( normal ), albedo.rgb, matSpecParams.rgb,
		                    matSpecParams.a, -vsPos.z, 0.3 );
}


[[FS_AMBIENT]]
// =================================================================================================

#ifdef _F03_ParallaxMapping
	#define _F02_NormalMapping
#endif

#include "shaders/utilityLib/fragLighting.glsl"

uniform vec4 matDiffuseCol;
uniform sampler2D albedoMap;
uniform samplerCube ambientMap;

#ifdef _F02_NormalMapping
	uniform sampler2D normalMap;
#endif

#ifdef _F04_EnvMapping
	uniform samplerCube envMap;
#endif

varying vec4 pos;
varying vec2 texCoords;

#ifdef _F02_NormalMapping
	varying mat3 tsbMat;
#else
	varying vec3 tsbNormal;
#endif

#ifdef _F03_ParallaxMapping
	uniform vec4 matParallaxParams;
	varying vec3 eyeTS;
#else
	#define newCoords texCoords
#endif

#ifdef _F04_EnvMapping
	uniform vec4 matReflParams;
	uniform vec4 matReflSpreadParams;
#endif

void main( void )
{
#ifdef _F03_ParallaxMapping
	vec3 newCoords = vec3( texCoords, 0 );

	// Iterative parallax mapping
	vec3 eye = normalize( eyeTS );
	for( int i = 0; i < 4; ++i )
	{
		vec4 nmap = texture2D( normalMap, newCoords.st );
		float height = nmap.a * matParallaxParams.b + matParallaxParams.b;
		newCoords += (height - newCoords.p) * nmap.z * eye;
	}
#endif

	vec4 albedo = texture2D( albedoMap, newCoords.st ) * matDiffuseCol;

#ifdef _F05_AlphaTest
	if( albedo.a < 0.01 ) discard;
#endif

#ifdef _F02_NormalMapping
	vec3 normalMap = texture2D( normalMap, newCoords.st ).rgb * 2.0 - 1.0;
	vec3 normal = tsbMat * normalMap;
#else
	vec3 normal = tsbNormal;
#endif

	vec4 ambient = textureCube( ambientMap, normal );
	gl_FragColor.rgb = albedo.rgb * ambient.rgb * (ambient.a + 1.0) / 2.0;

#ifdef _F04_EnvMapping
	int diffusion = int(matReflParams.a);

	vec3 refl = textureCube( envMap, reflect( pos.xyz - viewerPos, normalize( normal ) ) ).rgb;

	if(diffusion > 0)
	{
		// Manually-implemented multisampling!
		vec3 diffusionSpreadU = vec3(matReflSpreadParams.s, 0, matReflSpreadParams.t);
		vec3 diffusionSpreadW = vec3(matReflSpreadParams.p, 0, matReflSpreadParams.q);

		for(int j = 0; j < diffusion; j++)
		{
			refl += textureCube( envMap, reflect( pos.xyz - viewerPos, normalize( normal ) )
					+ float(j + 1) * diffusionSpreadW ).rgb
				+ textureCube( envMap, reflect( pos.xyz - viewerPos, normalize( normal ) )
					- float(j + 1) * diffusionSpreadW ).rgb;
		}

		for(int i = 0; i < diffusion; i++)
		{
			refl += textureCube( envMap, reflect( pos.xyz - viewerPos, normalize( normal ) )
					+ float(i + 1) * diffusionSpreadU ).rgb
				+ textureCube( envMap, reflect( pos.xyz - viewerPos, normalize( normal ) )
					- float(i + 1) * diffusionSpreadU ).rgb;

			for(int j = 0; j < diffusion; j++)
			{
				refl += textureCube( envMap, reflect( pos.xyz - viewerPos, normalize( normal ) )
						+ float(i + 1) * diffusionSpreadU + float(j + 1) * diffusionSpreadW ).rgb
					+ textureCube( envMap, reflect( pos.xyz - viewerPos, normalize( normal ) )
						+ float(i + 1) * diffusionSpreadU - float(j + 1) * diffusionSpreadW ).rgb

					+ textureCube( envMap, reflect( pos.xyz - viewerPos, normalize( normal ) )
						- float(i + 1) * diffusionSpreadU + float(j + 1) * diffusionSpreadW ).rgb
					+ textureCube( envMap, reflect( pos.xyz - viewerPos, normalize( normal ) )
						- float(i + 1) * diffusionSpreadU - float(j + 1) * diffusionSpreadW ).rgb;
			}
		}

		gl_FragColor.rgb += matReflParams.rgb * refl / float((diffusion + 1) * (diffusion + 1) * 4);
	}
	else
	{
		gl_FragColor.rgb += matReflParams.rgb * refl;
	}
#endif
}
